package com.tp.spark.utils

import play.api.libs.json.{JsError, JsSuccess, Json}
	
object TweetUtils {
	case class Tweet (
		id : String,
		user : String,
		text : String,
		place : String,
		country : String,
		lang : Option[String]
    )

    implicit val tweetFormat = Json.format[Tweet]
    
    def StringToTweet(str: String) = Json.parse(str).validate[Tweet] match {
      case JsError(e) => println(str); println(e); None
      case JsSuccess(t, _) => Some(t)
    }
}
