package com.tp.spark.core

import org.apache.spark.{SparkConf, SparkContext}
import org.apache.spark.sql._

object Ex0Wordcount {

  val pathToFile = "data/wordcount.txt"

  val conf = new SparkConf()
                      .setAppName("Wordcount")
                      .setMaster("local[*]")
  val session = SparkSession.builder.config(conf).getOrCreate
  import session.implicits._

  def loadData(): DataFrame = {
    SparkContext.getOrCreate(conf)
                .textFile(pathToFile)
                .flatMap(_.split(" "))
                .toDF("word")
  }

  def wordcount(): DataFrame = loadData.groupBy($"word").count

  def filterOnWordcount(): DataFrame = wordcount.filter("count > 4")
}
