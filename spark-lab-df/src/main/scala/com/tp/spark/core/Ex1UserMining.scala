package com.tp.spark.core

import org.apache.spark.SparkConf
import org.apache.spark.sql._

object Ex1UserMining {

  val pathToFile = "data/reduced-tweets.json"

  val conf = new SparkConf()
                      .setAppName("User mining")
                      .setMaster("local[*]")
  val session = SparkSession.builder.config(conf).getOrCreate
  import session.implicits._

  def loadData(): DataFrame = {
    session.read.json(pathToFile)
  }

  def tweetsByUser() = {
    // not used, actually quite ugly because it is not suited to dfs
    loadData.groupBy($"user")
            .agg(functions.collect_list($"id").alias("tweets"))
  }

  def tweetByUserNumber(): DataFrame = {
    loadData.groupBy($"user").count
  }

  def topTenTwitterers(): Array[(String, Int)] = {
    tweetByUserNumber.orderBy(functions.desc("count")).limit(10).collect.map {
      row => (row.getAs[String](0), row.getAs[Int](1))
    }
  }
}

