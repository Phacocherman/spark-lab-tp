package com.tp.spark.core

import org.apache.spark.SparkConf
import org.apache.spark.sql._

object Ex2TweetMining {

  val pathToFile = "data/reduced-tweets.json"

  val conf = new SparkConf()
                    .setAppName("Tweet mining")
                    .setMaster("local[*]")
  val session = SparkSession.builder.config(conf).getOrCreate
  import session.implicits._

  def loadData(): DataFrame = {
    session.read.json(pathToFile)
  }

  def mentionOnTweet(): DataFrame = {
    loadData.rdd.flatMap { tw: Row =>
      tw.getAs[String]("text").split(" ").filter { word: String =>
        word.startsWith("@") && word.length > 1
      }
    }.toDF("mention")
  }

  def countMentions(): DataFrame = {
    mentionOnTweet.groupBy($"mention").count
  }

  def top10mentions(): Array[(String, Int)] = {
    countMentions.orderBy(functions.desc("count")).limit(10).collect.map {
      row => (row.getAs[String](0), row.getAs[Int](1))
    }
  }
}
